# cnode-api

restful api

### Development
```bash
$ npm install
$ npm run dev
```

Then you can test by visiting: http://127.0.0.1:7001/api/v2/topics


### npm scripts

- Use `npm run lint` to check code style.
- Use `npm test` to run unit test.
- Use `npm run autod` to auto detect dependencies upgrade, see [autod](https://www.npmjs.com/package/autod) for more detail.

[mocha]: http://mochajs.org
[thunk-mocha]: https://npmjs.com/thunk-mocha
[power-assert]: https://github.com/power-assert-js/power-assert
[istanbul]: https://github.com/gotwarlost/istanbul
[egg-bin]: https://github.com/eggjs/egg-bin